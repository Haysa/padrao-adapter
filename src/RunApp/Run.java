package RunApp;

import RegraAdapterLadraoComPlastica.CirurgiaPlastica;
import RegraAdapterLadraoComPlastica.LadraoProcurado;
import RegraAdapterLadraoComPlastica.LadraoProcuradoComPlastica;
import RegraAdapterLadraoComPlastica.MedicoCirurgiao;
import RegraAdapterLadraoComPlastica.SalaDeCirurgia;

public class Run {

	public static void main(String[] args) {
		
		MedicoCirurgiao medico = new LadraoProcurado();
		LadraoProcuradoComPlastica ladrao = new LadraoProcuradoComPlastica();
		System.out.println("  ## Uma cirurgia pl�stica em um ladr�o altamente procurado. Ningu�m vai reconhec�-lo. ## \n");
		medico = new CirurgiaPlastica(ladrao);
		SalaDeCirurgia sala = new SalaDeCirurgia(medico);
		sala.fazerCirurgiaPlastica();
		System.out.println("  ## Fim da cirurgia e sucesso: NINGU�M O RECONHECEU. ##");
	}
}
