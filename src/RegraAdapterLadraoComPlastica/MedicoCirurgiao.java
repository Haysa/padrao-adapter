package RegraAdapterLadraoComPlastica;

public interface MedicoCirurgiao {

	void mudarCorDosOlhos();
	void mudarFormatoDoRosto();
	void fazerImplanteDoCabelo();
	void mudarFormaDoNariz();
	
}
