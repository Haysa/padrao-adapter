package RegraAdapterLadraoComPlastica;

public class SalaDeCirurgia {

	private MedicoCirurgiao medico;

	public SalaDeCirurgia(MedicoCirurgiao medico) {
		this.medico = medico;
	}

	public void fazerCirurgiaPlastica() {
		medico.fazerImplanteDoCabelo();
		medico.mudarFormatoDoRosto();
		medico.mudarFormaDoNariz();
		medico.mudarCorDosOlhos();

	}

}
