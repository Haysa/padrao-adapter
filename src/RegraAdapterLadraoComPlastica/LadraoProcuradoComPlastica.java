package RegraAdapterLadraoComPlastica;

public class LadraoProcuradoComPlastica {

	public void mudandoMeusOlhos() {
		System.out
				.println("Com a cirurgia pl�stica eu mudei a cor dos meus olhos... \n");
	}

	public void meuNarizVirouOutro() {
		System.out
				.println("Agora com o nariz altamente modificado eu posso parecer mais bonito e ningu�m vai me reconhecer.\n");
	}

	public void formatoDoMeuRosto() {
		System.out
				.println("O meu m�dico cirurgi�o mudou o formato do meu rosto e agora ningu�m vai me reconher. Muito menos a pol�cia...\n");
	}

	public void colocarMaisCabelo() {
		System.out
				.println("Agora tenho mais cabelos depois da cirurgia. Eu era um bandido careca e agora tenho cabelos.\n");
	}
}
