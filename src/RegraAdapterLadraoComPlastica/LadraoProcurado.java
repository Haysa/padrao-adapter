package RegraAdapterLadraoComPlastica;

public class LadraoProcurado implements MedicoCirurgiao {

	@Override
	public void mudarCorDosOlhos() {
		System.out
				.println("Sou ladr�o procurado e preciso mudar a cor dos meus olhos para n�o ser reconhecido.");

	}

	@Override
	public void mudarFormatoDoRosto() {
		System.out
				.println("Sou ladr�o procurado e preciso mudar drasticamente o formato do meu rosto.");

	}

	@Override
	public void fazerImplanteDoCabelo() {
		System.out
				.println("Sou ladr�o procurado e preciso colocar mais cabelo pois n�o serei mais conhecido por ser careca..antigamente.");

	}

	@Override
	public void mudarFormaDoNariz() {
		System.out
				.println("Sou ladr�o procurado e preciso mudar o nariz drasticamente mesmo...al�m de tudo.");
	}

}
