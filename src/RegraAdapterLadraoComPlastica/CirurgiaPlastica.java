package RegraAdapterLadraoComPlastica;

public class CirurgiaPlastica implements MedicoCirurgiao {

	private LadraoProcuradoComPlastica ladrao;

	public CirurgiaPlastica(LadraoProcuradoComPlastica ladrao) {
		this.ladrao = ladrao;
	}

	@Override
	public void mudarCorDosOlhos() {
		ladrao.mudandoMeusOlhos();
	}

	@Override
	public void mudarFormatoDoRosto() {
		ladrao.formatoDoMeuRosto();

	}

	@Override
	public void fazerImplanteDoCabelo() {
		ladrao.colocarMaisCabelo();

	}

	@Override
	public void mudarFormaDoNariz() {
		ladrao.meuNarizVirouOutro();

	}

}
